readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

dot_file_path=$1
names=$(bash $SCRIPT_DIR/split_dot.sh $dot_file_path);
echo $names
pngs=""
set -e
for name in $names; do
  dot -Tpng $name.dot -o $name.png
  pngs="$pngs $name.png"
done

open $pngs

