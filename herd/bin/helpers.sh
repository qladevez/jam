HELPER_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
HERD_DIR="$HELPER_SCRIPT_DIR/.."
LITMUS_DIR="$HERD_DIR/$MODEL/litmus"
MODELS_DIR="$HERD_DIR/$MODEL/models"
CONFIG_DIR="$HERD_DIR/$MODEL/config"
OUTPUT_DIR="$HERD_DIR/scratch"
MODEL_OUTPUT_DIR="$HERD_DIR/scratch/$MODEL"
JAM_OUTPUT_DIR="$HERD_DIR/scratch/jam"

if ! which herd7 > /dev/null; then
  echo "ERROR: The tests require that the herd7 binary is available in the path." >&2
  exit 1;
fi

mkdir -p "$MODEL_OUTPUT_DIR"
mkdir -p "$JAM_OUTPUT_DIR"

function herd_simple(){
  pushd "$MODELS_DIR" > /dev/null
  args=("$@")
  rest=${args[@]:2}
  timeouts=300
  memkb=5000000
  echo "HERD: model $1 for test $2 with args: $rest" >&2

  # NOTE timeout util not coreutils timeout, see `$PROJECT_ROOT/bin/vm-user.sh`
  # for install
  timeout -c \
          -t $timeouts \
          -m  $memkb \
          herd7 "$LITMUS_DIR/$2" \
          -candidates true \
          -conf "$CONFIG_DIR/$1.cfg" \
          -o "$OUTPUT_DIR/$1/" \
          $rest 2>&1

  # handle the exit status
  test_status=$?
  if [ "$test_status" != "0" ]; then
    echo "TIMEOUT/OOM, took longer than $timeouts seconds or exceeded $memkb kb memory." >&2;
  fi

  popd > /dev/null
}
export -f herd_simple


function loud_exit (){
  echo "FAILURE"
  exit 1
}
export -f loud_exit

function herd_clean (){
  args=("$@")
  rest=${args[@]:2}
  # NOTE find the observation output and remove the number of executions
  # we only care whether the observation is Never or Sometimes
  herd_simple $1 $2 $rest | grep Observation
}
export -f herd_clean


function diff_check() {
  diff $1 $2 | grep -v Time | grep "\(<\|>\)"
}
export -f diff_check
