readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
readonly PROJECT_DIR="$SCRIPT_DIR/.."
readonly SCRATCH_DIR="$PROJECT_DIR/scratch"
readonly COQ_DIR="$PROJECT_DIR/coq"

mkdir -p "$SCRATCH_DIR"
temp_dir=$(mktemp -d $SCRATCH_DIR/supplement.XXXX);

# copy the readme
cp "$PROJECT_DIR/README.md" "$temp_dir"

# copy everything, remove scratch and misc below
cp -r "$PROJECT_DIR/herd" "$temp_dir"

set -e

pushd "$temp_dir/"

# remove miscellaneous tests
rm -r "herd/rc11/litmus/misc"
rm -r "herd/aarch64/litmus/misc"

# sanity check that the tests can run
bash herd/bin/test.sh

# remove scratch dir created by cp of herd dir and sanity check
rm -r "herd/scratch"
popd

# copy coq source and project file
mkdir -p "$temp_dir/coq"
cp -r $PROJECT_DIR/coq/* "$temp_dir/coq/"
cp -r "$PROJECT_DIR/coq/_CoqProject" "$temp_dir/coq/"

# check for any admits
if grep -ri "admit" "$temp_dir/coq"; then
  echo "ERROR: something has been admitted in the source" >&2
  exit 1;
fi

# check the proofs
pushd "$temp_dir/coq"

# check that it builds according to the instructions
coq_makefile -f _CoqProject -o Makefile
make clean all
make clean

tree "$temp_dir"
tar -cf "$SCRATCH_DIR/supplement.tar" -C "$temp_dir" coq herd README.md

cp "$PROJECT_DIR/notes/tex/main.pdf" "$SCRATCH_DIR/oopsla-2019.pdf"
